#pragma once
#include <boost/asio.hpp>
#include "socketCom.h"
#include "../artificialInteligence/artificialInteligence.h"

using boost::asio::ip::tcp;

/// starts a server that will be callable to contact the given AI
void runServer(ArtificialIntelligence& ai, const int port, const bool shouldDisplay = true)
{
    try
    {
        boost::asio::io_context io_context;

        // listen to new connections on the given port, with the given protocol
        auto tcpProtocol = tcp::v4();
        tcp::acceptor acceptor(io_context, tcp::endpoint(tcpProtocol, port));

        // answers one connection at a time
        while(true)
        {
            // represents the connection from the client
            tcp::socket socket(io_context);
            // wait for a connection
            acceptor.accept(socket);

            // the message
            const auto board = Com::fromSocket<Board>(socket);
            if(shouldDisplay) Display::board(board);

            // our answer
            const Move move = ai.getMove(board);
            if(shouldDisplay) std::cout << "Playing [row:" << move.row << " column:" << move.col << "]" << std::endl;

            // sends our answer
            Com::toSocket(socket, move);
        }
    }
    catch (std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }

}
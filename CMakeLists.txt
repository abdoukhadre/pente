cmake_minimum_required(VERSION 3.17)
project(pente)

# libraries
#include_directories(libraries/???)

# flags
set(CMAKE_CXX_STANDARD 17)
add_definitions("-mfma -O3 -march=native -fopenmp
                 -Werror -Wall -Wextra -D__STRICT_ANSI__
                 -Wno-unused-parameter -Wno-unused-result -Wno-unused-variable")

# files
set(SOURCE_FILES src/main.cpp
        # artificial inteligences
        src/artificialInteligence/artificialInteligence.h
        src/artificialInteligence/humanAI.h
        # game implementation
        src/gameRules/types.h
        src/gameRules/board.h
        src/gameRules/gameLoop.h
        src/gameRules/utilities/display.h
        src/gameRules/utilities/timer.h
        # communications
        src/communications/server.h
        src/communications/clientAI.h
        src/communications/socketCom.h
        src/communications/serialize.h)

# linking
add_executable(pente.exe ${SOURCE_FILES})
target_link_libraries(pente.exe PUBLIC gomp pthread)
